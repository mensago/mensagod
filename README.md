# mensagod: server-side daemon for the [Mensago](https://mensago.org) online communications platform

**mensagod** (pronounced *mehn-sa-go-DEE*) provides identity services and data storage and synchronization for Mensago client applications. It is written in Kotlin and is released under the MIT license. 

## Description

This is the reference implementation of the server for the Mensago platform. Its role is to provide
strong identity assurances, digital certificate hosting, message delivery services, and user device
synchronization. The server daemon isn't dramatically different from other database-based
applications. It sits on top of PostgreSQL, runs as a non-privileged user, stores files in a
dedicated directory, and listens on the network.

## Contributing

Although a mirror repository can be found on GitHub for historical reasons, the official repository for this project is on [GitLab](https://gitlab.com/mensago/mensagod). Please submit issues and pull requests there.

Mensago itself is a very young, very ambitious project that needs help in all sorts of areas -- not just writing code. Find out more information at https://mensago.org/develop.

## Current Status and Roadmap

As of 4/2024, the server was recently rewritten in Kotlin and has reached feature parity with the
previous Go implementation, providing local message delivery and basic file synchronization.

Features needed to bring it to a feature-complete level include:

- Message delivery between servers
- Finish key lifecycle (rotation, revocation, etc.)
- Setup mode
