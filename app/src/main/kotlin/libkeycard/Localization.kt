package libkeycard

import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId
import java.time.ZoneOffset
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle

fun convertISO8601(s: String, formatter: DateTimeFormatter? = null)
        : Result<String> {
    return runCatching {
        LocalDateTime.ofInstant(Instant.parse(s), ZoneId.systemDefault())
            .format(formatter ?: DateTimeFormats.shortLocalDateTime)
    }
}

object DateTimeFormats {
    // "1/28/25, 12:24PM"
    val shortLocalDateTime = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.SHORT)!!

    // "Jan 28, 2025, 12:24PM"
    val medLocalDateTime = DateTimeFormatter.ofLocalizedDateTime(FormatStyle.MEDIUM)!!

    // "2025-01-28"
    val keycardDate = DateTimeFormatter.ofPattern("yyyy-MM-dd").withZone(ZoneOffset.UTC)!!
}
