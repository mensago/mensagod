package mensagod

/**
 * Singleton which contains application-level options.
 */
object StartupOptions {
    /**
     * debugMode is set to true if Connect is started in Debug Mode due to either -debug being
     * passed on the command line or if the environment variable MCONNECT_DEBUG is set to 1.
     *
     * In debug mode, Connect will enable debug DNS, which returns hardcoded DNS lookups for the
     * domain `example.com`.
     */
    var debugMode: Boolean = false
        private set
    var testMode: Boolean = false
        private set
    var useWeakPasswordHashing: Boolean = false
        private set

    /**
     * Initializes the application state options based on environment variables and command-line
     * arguments.
     */
    fun init(args: Array<String>): Int {
        if (args.isEmpty()) return 0

        val switches = mutableMapOf<String, Boolean>()
        for (arg in args) {
            when (arg.lowercase()) {
                "--debug", "-d" -> switches["debug"] = true
                "--test", "-t" -> switches["test"] = true
                "--weakhashes" -> switches["weakhashes"] = true
                "--help" -> switches["showhelp"] = true
                else -> {
                    println("Invalid option $arg")
                    switches["showhelp"] = true
                    break
                }
            }
        }

        if (switches["showhelp"] != null) {
            printHelp()
            return 1
        }

        debugMode = switches["debug"] != null
        if (switches["test"] != null) {
            testMode = true
            debugMode = true
        } else testMode = false

        useWeakPasswordHashing = switches["weakhashes"] != null

        val envDebug = System.getenv("MENSAGOD_DEBUG")
        if (envDebug != null && envDebug == "1")
            debugMode = true

        if (useWeakPasswordHashing && !testMode) {
            println("Weak password hashing requires test mode")
            return -1
        }

        if (testMode) {
            if (useWeakPasswordHashing)
                println("mensagod is running in test mode with weak password hashing")
            else
                println("mensagod is running in test mode")
        } else if (debugMode) println("mensagod is running in debug mode")

        return 0
    }

    private fun printHelp() {
        println("Usage: mensagod [option1 [option2...]]\n")
        println(
            "  --help, -h    Show this help information\n" +
                    "  --debug, -d   Enable debug mode, which enables additional diagnostic logging\n" +
                    "  --test, -t    Enable test mode, which also enables debug mode. Only for " +
                    "developers.\n" +
                    "  --weakhashes  Enable weak password hashing. Requires test mode. Use this only " +
                    "if you know what you are doing.\n"
        )
    }
}
