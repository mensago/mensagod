package mensagod.dbcmds

import keznacl.BadValueException
import keznacl.toFailure
import keznacl.toSuccess
import libkeycard.EntryTypeException
import libkeycard.RandomID
import libkeycard.WAddress
import libmensago.SyncItem
import libmensago.SyncOp
import libmensago.SyncValue
import mensagod.DBConn
import mensagod.DatabaseCorruptionException

/**
 * Adds a record to the update table.
 *
 * @throws BadValueException Returned if the record's data is invalid for the update type
 * @throws EntryTypeException Returned if there is a typing problem in the record's data field, such
 * as a directory being given for a Create update record.

 * @see SyncRecord
 */
fun addSyncRecord(db: DBConn, wid: RandomID, rec: SyncItem): Throwable? {

    when (rec.op) {
        SyncOp.Create -> {
            rec.value as SyncValue.Create
            if (rec.value.serverpath.isDir()) return EntryTypeException()
        }

        SyncOp.Delete -> {
            rec.value as SyncValue.Delete
            if (rec.value.serverpath.isDir()) return EntryTypeException()
        }

        SyncOp.Rotate -> {
            rec.value as SyncValue.Rotate
            if (rec.value.serverpath.isDir()) return EntryTypeException()
        }

        SyncOp.Mkdir -> {
            rec.value as SyncValue.Mkdir
            if (rec.value.serverpath.isFile()) return EntryTypeException()
        }

        SyncOp.Move -> {
            rec.value as SyncValue.Move
            if (rec.value.src.isDir() || rec.value.dest.isFile()) return EntryTypeException()
        }

        SyncOp.Receive -> {
            rec.value as SyncValue.Receive
            if (rec.value.serverpath.isDir()) return EntryTypeException()
        }

        SyncOp.Replace -> {
            rec.value as SyncValue.Replace
            if (rec.value.oldfile.isDir() || rec.value.newfile.isDir()) return EntryTypeException()
        }

        SyncOp.Rmdir -> {
            rec.value as SyncValue.Rmdir
            if (rec.value.serverpath.isFile()) return EntryTypeException()
        }
    }

    return db.execute(
        """INSERT INTO updates(rid,wid,domain,update_type,update_data,unixtime,devid)
        VALUES(?,?,?,?,?,?,?)""",
        rec.id, wid, rec.waddress.domain, rec.op, rec.value, rec.unixtime, rec.source
    ).exceptionOrNull()
}

/**
 * Returns the number of sync records which occurred after the specified time with a maximum of 1000
 *
 * @throws BadValueException Returned if unixtime is negative
 */
fun countSyncRecords(db: DBConn, wid: RandomID, unixtime: Long): Result<Int> {
    if (unixtime < 0) return BadValueException().toFailure()

    val rs = db.query(
        "SELECT COUNT(*) FROM updates WHERE wid=? AND unixtime >= ?",
        wid, unixtime
    ).getOrElse { return it.toFailure() }
    rs.next()
    return rs.getInt(1).toSuccess()
}

/**
 * Clears out old sync records older than the time specified.
 */
fun cullOldSyncRecords(db: DBConn, unixtime: Long): Throwable? {
    return db.execute("DELETE FROM updates WHERE unixtime < ?", unixtime).exceptionOrNull()
}

/**
 * Gets all update records after a specified period of time up to a maximum of 1000.
 */
fun getSyncRecords(db: DBConn, waddr: WAddress, unixtime: Long): Result<List<SyncItem>> {
    if (unixtime < 0) return BadValueException().toFailure()

    val rs = db.query(
        "SELECT * FROM updates WHERE wid=? AND domain=? AND unixtime >= ? " +
                "ORDER BY rowid LIMIT 1000",
        waddr.id, waddr.domain, unixtime
    )
        .getOrElse { return it.toFailure() }
    val out = mutableListOf<SyncItem>()
    while (rs.next()) {
        val id = RandomID.fromString(rs.getString("rid"))
            ?: return DatabaseCorruptionException("Invalid update ID").toFailure()
        val type = SyncOp.fromString(rs.getString("update_type"))
            ?: return DatabaseCorruptionException("Invalid update type").toFailure()
        val devid = RandomID.fromString(rs.getString("devid"))
            ?: return DatabaseCorruptionException("Invalid device ID").toFailure()
        val value = SyncValue.fromString(type, rs.getString("update_data"))
            .getOrElse { return it.toFailure() }

        out.add(
            SyncItem(
                id, type, waddr, value, rs.getLong("unixtime"),
                devid
            )
        )
    }
    return out.toSuccess()
}
