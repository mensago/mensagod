package libmensago

enum class WorkspaceType {
    Individual,
    Shared;

    override fun toString(): String {
        return when (this) {
            Individual -> "individual"
            Shared -> "shared"
        }
    }

    companion object {

        fun fromString(s: String): WorkspaceType? {
            return when (s.lowercase()) {
                "individual" -> Individual
                "shared" -> Shared
                else -> null
            }
        }
    }
}

enum class WorkspaceStatus {
    Active,
    Approved,
    Archived,
    BalanceDue,
    Blocked,
    Disabled,
    Local,
    Pending,
    Preregistered,
    Suspended,
    Unknown;

    override fun toString(): String {
        return when (this) {
            Active -> "active"
            Approved -> "approved"
            Archived -> "archived"
            BalanceDue -> "balancedue"
            Blocked -> "blocked"
            Disabled -> "disabled"
            Local -> "local"
            Pending -> "pending"
            Preregistered -> "preregistered"
            Suspended -> "suspended"
            Unknown -> "unknown"
        }
    }

    companion object {

        fun fromString(s: String): WorkspaceStatus? {
            return when (s.lowercase()) {
                "active" -> Active
                "approved" -> Approved
                "archived" -> Archived
                "balancedue" -> BalanceDue
                "blocked" -> Blocked
                "disabled" -> Disabled
                "local" -> Local
                "pending" -> Pending
                "preregistered" -> Preregistered
                "suspended" -> Suspended
                "unknown" -> Unknown
                else -> null
            }
        }

    }
}
