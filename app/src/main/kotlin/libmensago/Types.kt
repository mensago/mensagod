package libmensago

/**
 * The ContentFormat class represents the text format contained in a message. Currently the only
 * supported formats are plaintext and SDF.
 */
enum class ContentFormat {
    Text,
    SDF;

    override fun toString(): String {
        return when (this) {
            Text -> "text"
            SDF -> "sdf"
        }
    }

    companion object {
        fun fromString(s: String?): ContentFormat? {
            if (s == null) return null
            return when (s.lowercase()) {
                "text" -> Text
                "sdf" -> SDF
                else -> null
            }
        }
    }
}

/** The DeviceStatus class represent's a device activation status in a workspace. */
enum class DeviceStatus {
    Approved,
    Blocked,
    NotRegistered,
    Pending,
    Registered;

    override fun toString(): String {
        return when (this) {
            Approved -> "approved"
            Blocked -> "blocked"
            NotRegistered -> "notregistered"
            Pending -> "pending"
            Registered -> "registered"
        }
    }

    companion object {

        fun fromString(s: String): DeviceStatus? {
            return when (s.lowercase()) {
                "approved" -> Approved
                "blocked" -> Blocked
                "notregistered" -> NotRegistered
                "pending" -> Pending
                "registered" -> Registered
                else -> null
            }
        }

    }
}

/** Enumeration which represents the type of quoting to be used in generating message replies */
enum class QuoteType {
    None,
    Top,
    Bottom,
}

enum class ContactRequestAction {
    Approve,
    Reject,
    Block;

    override fun toString(): String {
        return when (this) {
            Approve -> "approve"
            Reject -> "reject"
            Block -> "block"
        }
    }

    companion object {
        fun fromString(s: String): ContactRequestAction? {
            return when (s.lowercase()) {
                "approve" -> Approve
                "reject" -> Reject
                "block" -> Block
                else -> null
            }
        }
    }
}

enum class DeviceRequestAction {
    Approve,
    Reject;

    override fun toString(): String {
        return when (this) {
            Approve -> "approve"
            Reject -> "reject"
        }
    }

    companion object {
        fun fromString(s: String): DeviceRequestAction? {
            return when (s.lowercase()) {
                "approve" -> Approve
                "reject" -> Reject
                else -> null
            }
        }
    }
}
