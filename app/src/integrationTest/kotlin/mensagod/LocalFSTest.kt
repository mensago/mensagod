package mensagod

import libkeycard.RandomID
import libmensago.MServerPath
import org.apache.commons.io.FileUtils
import org.junit.jupiter.api.Assertions.assertEquals
import org.junit.jupiter.api.Assertions.assertFalse
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.assertThrows
import testsupport.ADMIN_PROFILE_DATA
import testsupport.makeTestFile
import testsupport.setupTest
import java.io.File
import java.nio.file.Paths

class LocalFSTest {

    @Test
    fun convertTest() {
        setupTest("fs.convertTest")
        val lfs = LocalFS.get()

        val converted = Paths.get(
            lfs.basePath.toString(), "d8b6d06b-7728-4c43-bc08-85a0c645d260", "tasks"
        ).toString()

        assertEquals(
            converted, lfs.convertToLocal(
                MServerPath("/ d8b6d06b-7728-4c43-bc08-85a0c645d260 tasks")
            ).toString()
        )
    }

    @Test
    fun listTest() {
        val setupData = setupTest("fs.list")
        val one = "11111111-1111-1111-1111-111111111111"
        val two = "22222222-2222-2222-2222-222222222222"
        val three = "33333333-3333-3333-3333-333333333333"

        val rootdir = Paths.get(setupData.testPath, "topdir")
        val oneInfo = makeTestFile(
            rootdir.toString(),
            "1000000.1024.$one",
            1024
        )
        val twoInfo = makeTestFile(
            rootdir.toString(),
            "1000100.1024.$two",
            1024
        )
        val threeInfo = makeTestFile(
            rootdir.toString(),
            "1000200.1024.$three",
            1024
        )

        listOf("new", "out", "keys", "settings").forEach {
            Paths.get(rootdir.toString(), it).toFile().mkdirs()
        }

        val rootHandle = MServerPath().toHandle()

        val files = rootHandle.list(true).getOrThrow()
        assertEquals(3, files.size)
        assertEquals(oneInfo.first, files[0])
        assertEquals(twoInfo.first, files[1])
        assertEquals(threeInfo.first, files[2])

        val dirs = rootHandle.list(false).getOrThrow()
        assertEquals(4, dirs.size)
        assertEquals("keys", dirs[0])
        assertEquals("new", dirs[1])
        assertEquals("out", dirs[2])
        assertEquals("settings", dirs[3])
    }

    @Test
    fun existsDeleteFileTest() {
        val setupData = setupTest("fs.deleteFile")
        val lfs = LocalFS.get()

        val topdir = Paths.get(setupData.testPath, "topdir").toString()
        val testFileInfo = makeTestFile(topdir)

        val deletePath = MServerPath("/ ${testFileInfo.first}")
        val handle = lfs.entry(deletePath)
        assert(handle.exists().getOrThrow())
        lfs.lock(deletePath)
        handle.delete()?.let { throw it }
        lfs.unlock(deletePath)
        assertFalse(handle.exists().getOrThrow())
    }

    @Test
    fun makeDirTest() {
        val setupData = setupTest("fs.makeDirectory")
        val lfs = LocalFS.get()

        assertThrows<FSFailureException> {
            lfs.entry(MServerPath("/ 6e99f804-7bb6-435a-9dce-53d9c6d33816 new"))
                .makeDirectory()
                ?.let { throw it }
        }

        val wspdir = File(
            Paths.get(
                setupData.testPath, "topdir",
                "6e99f804-7bb6-435a-9dce-53d9c6d33816"
            ).toString()
        )
        assert(!wspdir.exists())
        lfs.entry(MServerPath("/ 6e99f804-7bb6-435a-9dce-53d9c6d33816"))
            .makeDirectory()
            ?.let { throw it }
        assert(wspdir.exists())
    }

    @Test
    fun makeTempTest() {
        setupTest("fs.makeTempFile")

        val adminWID = RandomID.fromString(ADMIN_PROFILE_DATA["wid"])!!
        val lfs = LocalFS.get()
        val tempHandle = lfs.makeTempFile(adminWID, 4096).getOrThrow()
        FileUtils.touch(tempHandle.getFile())
    }

    @Test
    fun moveCopyTest() {
        val setupData = setupTest("fs.moveCopy")
        val lfs = LocalFS.get()

        val widStr = "6e99f804-7bb6-435a-9dce-53d9c6d33816"
        lfs.entry(MServerPath("/ $widStr")).makeDirectory()?.let { throw it }
        val testFileName =
            makeTestFile(Paths.get(setupData.testPath, "topdir", widStr).toString()).first

        val movedEntry = lfs.entry(MServerPath("/ $widStr $testFileName"))
        movedEntry.moveTo(MServerPath("/"))?.let { throw it }
        assert(
            File(Paths.get(setupData.testPath, "topdir", testFileName).toString())
                .exists()
        )
        assertEquals("/ $testFileName", movedEntry.path.toString())

        val newFilePath = lfs.entry(MServerPath("/ $testFileName"))
            .copyTo(MServerPath("/ $widStr")).getOrThrow()
        val newFileLocalPath = lfs.convertToLocal(newFilePath)
        assert(File(newFileLocalPath.toString()).exists())
    }
}