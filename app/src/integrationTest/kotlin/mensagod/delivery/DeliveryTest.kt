package mensagod

import keznacl.EncryptionPair
import libkeycard.WAddress
import libmensago.*
import libmensago.resolver.KCResolver
import mensagod.delivery.queueMessageForDelivery
import org.junit.jupiter.api.Test
import testsupport.ADMIN_PROFILE_DATA
import testsupport.setupTest
import java.lang.Thread.sleep

class DeliveryTest {

    @Test
    fun basicTest() {
        val setupData = setupTest("delivery.basic")
        KCResolver.dns = FakeDNSHandler(setupData.config.connectToDB().getOrThrow())

        val adminAddr = WAddress.fromString(ADMIN_PROFILE_DATA["waddress"])!!

        val msg = Message(gServerAddress, adminAddr, ContentFormat.Text)
            .withSubType("delivertyreport")
            .withBody("Delivery Report: External Delivery Not Implemented")
            .withSubject(
                "The server was unable to deliver your message because external delivery" +
                        "is not yet implemented. Sorry!"
            )


        val adminPair = EncryptionPair.fromStrings(
            ADMIN_PROFILE_DATA["encryption.public"]!!,
            ADMIN_PROFILE_DATA["encryption.private"]!!
        ).getOrThrow()

        val sealed = Envelope.seal(
            adminPair, AddressPair(msg.from, msg.to),
            msg.toPayload().getOrThrow()
        ).getOrThrow().toStringResult().getOrThrow()

        val lfs = LocalFS.get()
        lfs.entry(MServerPath("/ $gServerDevID")).makeDirectory()?.let { throw it }

        val adminOutPath = MServerPath("/ $gServerDevID out")

        val outHandle = lfs.entry(MServerPath("/ $gServerDevID out"))
        outHandle.makeDirectory()?.let { throw it }

        val handle = lfs.makeTempFile(gServerDevID, sealed.length.toLong()).getOrThrow()
        handle.getFile().writeText(sealed)
        handle.moveTo(adminOutPath)?.let { throw it }
        val thread = queueMessageForDelivery(gServerAddress, adminAddr.domain, handle.path)
        sleep(100)

        thread.join()
    }
}