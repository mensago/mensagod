package mensagod

import keznacl.SHAPassword
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

class PasswordFactoryTest {

    @Test
    fun basicTests() {
        assertEquals("ARGON2ID", PasswordFactory.defaultHasher.getAlgorithm())
        assert(PasswordFactory.getAlgorithms().contains("SHA3-256"))
        assert(PasswordFactory.setDefaultAlgorithm("SHA3-256"))

        assertEquals("SHA3-256", PasswordFactory.defaultHasher.getAlgorithm())
        val shaHasher = PasswordFactory.defaultHasher as SHAPassword
        shaHasher.setIterations(10)

        val hasherCopy = PasswordFactory.getHasher() as SHAPassword
        assertEquals(10, hasherCopy.getIterations())
    }
}